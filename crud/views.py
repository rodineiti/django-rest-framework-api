from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from crud.models import Crud
from crud.serializers import CrudSerializer
from rest_framework.decorators import api_view


@api_view(['GET', 'POST', 'DELETE'])
def crud_index(request):
    if request.method == 'GET':
        data = Crud.objects.all()
        
        title = request.GET.get('title', None)
        if title is not None:
            data = data.filter(title__icontains=title)
        
        data_serializer = CrudSerializer(data, many=True)
        return JsonResponse(data_serializer.data, safe=False)
        # 'safe=False' for objects serialization
 
    elif request.method == 'POST':
        list_data = JSONParser().parse(request)
        data_serializer = CrudSerializer(data=list_data)
        if data_serializer.is_valid():
            data_serializer.save()
            return JsonResponse(data_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(data_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    elif request.method == 'DELETE':
        count = Crud.objects.all().delete()
        return JsonResponse({'message': '{} deletados com sucesso!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)
 
 
@api_view(['GET', 'PUT', 'DELETE'])
def crud_show(request, pk):
    try: 
        item = Crud.objects.get(pk=pk) 
    except Crud.DoesNotExist: 
        return JsonResponse({'message': 'Post não existe'}, status=status.HTTP_404_NOT_FOUND) 
 
    if request.method == 'GET': 
        data_serializer = CrudSerializer(item) 
        return JsonResponse(data_serializer.data) 
 
    elif request.method == 'PUT': 
        list_data = JSONParser().parse(request) 
        data_serializer = CrudSerializer(item, data=list_data) 
        if data_serializer.is_valid(): 
            data_serializer.save() 
            return JsonResponse(data_serializer.data) 
        return JsonResponse(data_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 
    elif request.method == 'DELETE': 
        item.delete() 
        return JsonResponse({'message': 'Post deletado com sucesso!'}, status=status.HTTP_204_NO_CONTENT)
    
        
@api_view(['GET'])
def crud_index_published(request):
    data = Crud.objects.filter(published=True)
        
    if request.method == 'GET': 
        data_serializer = CrudSerializer(data, many=True)
        return JsonResponse(data_serializer.data, safe=False)