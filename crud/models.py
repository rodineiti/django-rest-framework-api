from django.db import models

# Create your models here.

class Crud(models.Model):
	title = models.CharField(max_length=70, blank=False, default='')
	body = models.CharField(max_length=200, blank=False, default='')
	published = models.BooleanField(default=False)
		