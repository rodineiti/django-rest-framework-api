from django.conf.urls import url
from crud import views

urlpatterns = [
    url(r'^api/post$', views.crud_index),
    url(r'^api/post/(?P<pk>[0-9]+)$', views.crud_show),
    url(r'^api/post/published$', views.crud_index_published)
]
