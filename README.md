# Django Restful CRUD API with Sqlite example

## Running the Application

Run the development web server:

```
python manage.py runserver 8000
```

Open the URL http://localhost:8000/ to access the application.